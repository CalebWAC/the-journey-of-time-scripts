using System.Collections;
using UnityEngine;

public class BeginGame : MonoBehaviour
{
	//public GameObject spinjitzu;
	public GameObject character;

	public GameObject count3;
	public GameObject count2;
	public GameObject count1;

	// Start is called before the first frame update
	void Start()
	{
		count3.SetActive(false);
		count2.SetActive(false);
		count1.SetActive(false);
		StartCoroutine(PlayTutorial());
	}

	// Update is called once per frame
	void Update()
	{

	}

	IEnumerator PlayTutorial()
	{
		count3.SetActive(true);
		yield return new WaitForSeconds(1);
		count3.SetActive(false);
		count2.SetActive(true);
		yield return new WaitForSeconds(1);
		count2.SetActive(false);
		count1.SetActive(true);
		yield return new WaitForSeconds(1);
		count1.SetActive(false);

		character.BroadcastMessage("Begin");

		Debug.Log("Complete!");
	}
}
