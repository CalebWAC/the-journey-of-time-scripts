using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Unity.LEGO.Minifig
{
    public class FollowPlayer : MonoBehaviour
    {
        public GameObject player;
        Vector3 initialOffset;
        Vector3 cameraPosition;
        public float smoothness;
        public string level = "Some Other Level";

        // Start is called before the first frame update
        void Start()
        {
            initialOffset = transform.position - player.transform.position;
        }

        // Update is called once per frame
        void Update() {
            if (level == "Some Other Level") {
                cameraPosition = new Vector3(player.transform.position.x, player.transform.position.y / 3, player.transform.position.z) + initialOffset;
                //cameraPosition = player.transform.position + initialOffset;
                transform.position = Vector3.Lerp(transform.position, cameraPosition, smoothness* Time.fixedDeltaTime);
                //transform.position = new Vector3(transform.position.x, 5.31f, transform.position.z);
            } else if (level == "Tutorial")
            {
                cameraPosition = new Vector3(player.transform.position.x, player.transform.position.y / 3, player.transform.position.z) + initialOffset;
                transform.position = Vector3.Lerp(transform.position, cameraPosition, smoothness * Time.fixedDeltaTime);
                transform.position = new Vector3(transform.position.x, 5.31f, transform.position.z);
            } else if (level == "Level Three") {
                cameraPosition = new Vector3(player.transform.position.x, player.transform.position.y / 1.5f, player.transform.position.z) + initialOffset;
                //cameraPosition = player.transform.position + initialOffset;
                transform.position = Vector3.Lerp(transform.position, cameraPosition, smoothness * Time.fixedDeltaTime);
                //transform.position = new Vector3(transform.position.x, 5.31f, transform.position.z);
            }
        }

        public void NewMe(GameObject newPlayer)
        {
            player = newPlayer;
        }
    }
}