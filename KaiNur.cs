using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Unity.LEGO.Minifig
{
    public class KaiNur : MonoBehaviour
    {
        public GameObject spinjitzu;
        private float startTime;
        public float waitTime = 1f;

        private Animator anim;
        public RuntimeAnimatorController original;
        public RuntimeAnimatorController spinning;

        private MinifigController mc;
        public float endPosition;

        public bool spinActive = true;
        public bool elementActive = false;
        private Vector3 spinInitPos;

        // Start is called before the first frame update
        void Start()
        {
            spinInitPos = spinjitzu.transform.position;
            anim = GetComponent<Animator>();
            mc = GetComponent<MinifigController>();
            StartCoroutine("Begin");
        }

        // Update is called once per frame
        void Update()
        {
            StartCoroutine("Begin");
        }

        IEnumerator Begin()
        {
            if (spinActive == true)
            {
                if (Input.GetKey("space") && Time.time - startTime > waitTime && !(mc.exploded))
                {
                    spinjitzu.transform.position = transform.position;
                }
                if (!(Input.GetKey("space")))
                {
                    //spinjitzu.transform.position = new Vector3(0, 1000, 0);
                    spinjitzu.transform.position = spinInitPos;
                }
            }

            yield return null;
        }
    }
}