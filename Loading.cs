using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Loading : MonoBehaviour
{
    // Start is called before the first frame update
    public string sceneName;
    AsyncOperation loadingOperation;
    //public Slider self;

    void Start()
    {
        loadingOperation = SceneManager.LoadSceneAsync(sceneName);
    }

    // Update is called once per frame
    void Update()
    {
        //self.value = Mathf.Clamp01(loadingOperation.progress / 0.9f);
    }
}
