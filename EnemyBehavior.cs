using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

namespace Unity.LEGO.Minifig
{
    public class EnemyBehavior : MonoBehaviour
    {
        private BoxCollider col;
        public GameObject self;
        public GameObject spinjitzu;
        public GameObject player;
        public Animator anim;
        public MinifigController mc;
        private bool playerHit = false;
        private bool secondTime = false;
        public AudioClip audioClipLose;
        public GameObject noiseMaker;
        //public Animation playerAnim;

        private bool saidIt = false;
        public string level = "Level 1";
        private Vector3 initSpinPos;

        void Start()
        {
            col = GetComponent<BoxCollider>();
            anim = GetComponent<Animator>();
            initSpinPos = spinjitzu.transform.position;
            Debug.Log(initSpinPos);
        }

        void Update()
        {
            if (playerHit)
            {
                player.transform.position = new Vector3(player.transform.position.x, player.transform.position.y - 1, player.transform.position.z - 0.25f);
            }

            if (secondTime && !playerHit)
            {
                Debug.Log("playerHit has been deactivated");
            }

            if (player.transform.position.y < -3 && !saidIt)
            {
                StartCoroutine(Begin());
                saidIt = true;
            }
        }

        void OnTriggerEnter(Collider col)
        {
            Debug.Log("Trigger has been activated");
            if (col.gameObject.CompareTag("Player") && /*(col.transform.position == spinjitzu.transform.position || (spinjitzu.transform.position.z < col.transform.position.z + 1 && spinjitzu.transform.position.z > col.transform.position.z - 1))*/ spinjitzu.transform.position != initSpinPos)
            {
                Debug.Log("I've been Spinjagoed");
                anim.Play("Flip_No_Y_Axis");
            }
            else if (col.gameObject.CompareTag("Player") && !(col.transform == spinjitzu.transform))
            {
                BroadcastMessage("Hit");
                // mc = player.GetComponent<MinifigController>();
                // mc.PlaySpecialAnimation(MinifigController.SpecialAnimation.Superman_Flying);
                // Animator playerAnim = player.GetComponent<Animator>();
                // anim.Play("Superman_Flying");
                mc = player.GetComponent<MinifigController>();
                mc.Explode();
                StartCoroutine(Begin());
            }
        }

        IEnumerator<WaitForSeconds> Begin()
        {
            //AudioSource audioSource = GetComponent<AudioSource>();
            //audioSource.PlayOneShot(audioClipLose, 1f);
            yield return new WaitForSeconds(2);
            // playerHit = true;
            //yield return new WaitForSeconds(1);
            // player.transform.position = new Vector3(244.2144f, 0.44f, -45.531f);
            Scene scene = SceneManager.GetActiveScene();
            SceneManager.LoadScene($"{scene.name} Loading");
            // noiseMaker.BroadcastMessage("PlaySound");
        }
    }
};