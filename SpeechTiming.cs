using System.Collections;
using UnityEngine;

public class SpeechTiming : MonoBehaviour
{
    public GameObject welcome;
    public GameObject intro;
    public GameObject oneButton;
    public GameObject jump;
    public GameObject nice;
    public GameObject spinjitzuTry;
    public GameObject impressive;
    public GameObject element1;
    public GameObject element2;
    public GameObject begin;

    public AudioClip spaceBar;
    public AudioClip tryStart;
    public AudioClip nicelyDone;
    public AudioClip impressiveNinja;

    public GameObject spinjitzu;
    public GameObject character;

    public GameObject count3;
    public GameObject count2;
    public GameObject count1;

    private AudioSource audio;
    

    // Start is called before the first frame update
    void Start()
    {
	intro.SetActive(false);
	oneButton.SetActive(false);
	jump.SetActive(false);
	count3.SetActive(false);
	count2.SetActive(false);
	count1.SetActive(false);
	StartCoroutine(PlayTutorial());
    }

    // Update is called once per frame
    void Update()
    {
		   
    }

    IEnumerator PlayTutorial() {
	// You're here! Good thing I finished my tea!
	audio = GetComponent<AudioSource>();
	welcome.SetActive(true); 
	yield return new WaitForSeconds(4);

	// To get back to the present, you need to go as fast as possible through time.
	welcome.SetActive(false); 
	intro.SetActive(true); 
	yield return new WaitForSeconds(6.5f);

	// All actions you can take can be taken with the space bar.
	audio.PlayOneShot(spaceBar, 1f);
	oneButton.SetActive(true);
	intro.SetActive(false);
	yield return new WaitForSeconds(4);

	// Let's start by pressing space.
	oneButton.SetActive(false);
	jump.SetActive(true);
	audio.PlayOneShot(tryStart, 1f);
	yield return new WaitUntil(() => Input.GetKey(KeyCode.Space));

	// Nicely Done!
	jump.SetActive(false);
	nice.SetActive(true);
	if (audio.isPlaying)
	{
		audio.Stop();
	}
	audio.PlayOneShot(nicelyDone, 1f);
	yield return new WaitForSeconds(2);

	// If you hold down the space bar, you can do spinjitzu. Try it out!
	nice.SetActive(false);
	spinjitzuTry.SetActive(true);
	yield return new WaitUntil(() => Input.GetKey(KeyCode.Space) && spinjitzu.transform.position == character.transform.position);

	// Impressive, young ninja.
	if (audio.isPlaying)
	{
		audio.Stop();
	}
	spinjitzuTry.SetActive(false);
	impressive.SetActive(true);
	audio.PlayOneShot(impressiveNinja, 1f);
	yield return new WaitForSeconds(2);
	
	// Travelling through time took away your elemental powers, 
	impressive.SetActive(false);
	element1.SetActive(true);
	yield return new WaitForSeconds(4);

	// but I am sure there are people you will meet that can still use theirs.
	element1.SetActive(false);
	element2.SetActive(true);
	yield return new WaitForSeconds(5);

	// Ready to begin?
	element2.SetActive(false);
	begin.SetActive(true);
	yield return new WaitForSeconds(2);
	begin.SetActive(false);

	count3.SetActive(true);
	yield return new WaitForSeconds(1);
	count3.SetActive(false);
	count2.SetActive(true);
	yield return new WaitForSeconds(1);
	count2.SetActive(false);
	count1.SetActive(true);
	yield return new WaitForSeconds(1);
	count1.SetActive(false);
	
	character.BroadcastMessage("Begin");

	Debug.Log("Complete!");
    }
}
